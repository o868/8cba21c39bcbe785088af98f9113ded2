import React from 'react';
import FavoritesPage from '../components/Favorites/Favorites';

const Favorites = () => {
  return <FavoritesPage />;
};

export default Favorites;
