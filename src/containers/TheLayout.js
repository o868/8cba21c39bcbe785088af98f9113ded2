import React, { useState } from 'react';
import Box from '../libraries/Box';
import HeaderMain from '../components/Layout/Header/Header';
import NavbarMain from '../components/Layout/Navbar/Navbar';
import { Redirect } from 'react-router-dom';
import Content from './Content';
import { useSelector } from 'react-redux';
import {
  AppShell,
  Navbar,
  Header,
  useMantineTheme,
  MediaQuery,
  Burger,
} from '@mantine/core';
import { lightTheme, darkTheme } from '../styles/theme';

const TheLayout = () => {
  const auth = useSelector((state) => state.auth);
  const themeBox = useSelector((state) => state.ui.theme);
  const theme = useMantineTheme();
  const [opened, setOpened] = useState(false);
  if (Box.auth.isValid && auth?.isValid) {
    return (
      <AppShell
        styles={() => ({
          main: {
            backgroundColor:
              themeBox === 'light' ? lightTheme.bg : darkTheme.bg,
          },
        })}
        navbarOffsetBreakpoint="sm"
        asideOffsetBreakpoint="sm"
        fixed
        navbar={
          <Navbar
            styles={{
              root: {
                backgroundColor:
                  themeBox === 'light' ? lightTheme.bg : darkTheme.bg,
              },
            }}
            p="md"
            hiddenBreakpoint="sm"
            hidden={!opened}
            width={{ sm: 200, lg: 300 }}
          >
            <NavbarMain />
          </Navbar>
        }
        header={
          <Header
            styles={{
              root: {
                backgroundColor:
                  themeBox === 'light' ? lightTheme.bg : darkTheme.bg,
              },
            }}
            height={60}
            p="md"
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                height: '100%,',
                width: '100%',
              }}
            >
              <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
                <Burger
                  opened={opened}
                  onClick={() => setOpened((o) => !o)}
                  size="sm"
                  color={theme.colors.gray[6]}
                  mr="xl"
                />
              </MediaQuery>
              <HeaderMain />
            </div>
          </Header>
        }
      >
        <Content />
      </AppShell>
      // <AppShell
      //   padding="md"
      //   navbar={
      //     <Navbar
      //       styles={{
      //         root: {
      //           backgroundColor:
      //           themeBox === 'light' ? lightTheme.bg : darkTheme.bg,
      //         },
      //       }}
      //       width={{ base: 300 }}
      //       p="xs"
      //     >
      //       <NavbarMain />
      //     </Navbar>
      //   }
      //   header={
      //     <Header sx={{ padding: '0px !important' }} height={60} p="xs">
      //       <HeaderMain />
      //     </Header>
      //   }
      //   styles={() => ({
      //     main: {
      //       backgroundColor: themeBox === 'light' ? lightTheme.bg : darkTheme.bg,
      //     },
      //   })}
      // >
      //   <Content />
      // </AppShell>
    );
  }
  return <Redirect exact push to="/login" />;
};

export default TheLayout;
