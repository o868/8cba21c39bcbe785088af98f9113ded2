import {
  Card,
  Grid,
  Image,
  Text,
  useMantineTheme,
  Group,
  Badge,
  Blockquote,
} from '@mantine/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { CurrencyDollar, Heart } from 'tabler-icons-react';
import { favoriteActions } from '../../app/slices/FavoriteSlices';
import { lightTheme } from '../../styles/theme';
import ButtonItem from './ButtonItem';

const CardItem = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const { item } = props;
  const favorites = useSelector((state) => state.favorite.favorites);
  const theme = useMantineTheme();
  const dispatch = useDispatch();

  const addFav = (item) => {
    dispatch(favoriteActions.addFavorite(item));
  };

  return (
    <Card
      sx={{
        backgroundColor:
          theme.colorScheme === 'dark' ? theme.colors.dark[1] : lightTheme.bg,
      }}
      shadow="sm"
      p="lg"
    >
      <Card.Section>
        <Image
          src={item?.image?.src}
          height={250}
          alt={item.title}
          fit="contain"
        />
      </Card.Section>
      <Group
        position="apart"
        style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
      >
        <Text weight={500}>{item?.title ?? ''}</Text>
        <Badge variant="light" color="green" text={item?.tags}>
          {item?.tags}
        </Badge>
      </Group>

      <Blockquote cite={t('discountPrice')} icon={<CurrencyDollar size={24} />}>
        {item?.variants[0].price}
      </Blockquote>

      <Grid>
        <Grid.Col span={6}>
          <ButtonItem
            variant="light"
            color="blue"
            fullWidth
            style={{ marginTop: 14 }}
            onClick={() => {
              history.push(`/detail/${item.id}`, { item: item });
            }}
            label={t('detail')}
          />
        </Grid.Col>
        <Grid.Col span={6}>
          <ButtonItem
            variant="light"
            color="pink"
            fullWidth
            style={{ marginTop: 14 }}
            onClick={() => {
              if (favorites.find((fav) => fav.id === item.id)) {
                dispatch(favoriteActions.removeFavorite(item));
              } else {
                addFav(item);
              }
            }}
            label={
              <Heart
                fill={
                  favorites.some((itemFav) => itemFav.id === item.id)
                    ? 'red'
                    : 'none'
                }
                color="red"
                size={16}
              />
            }
          ></ButtonItem>
        </Grid.Col>
      </Grid>
    </Card>
  );
};

export default CardItem;
