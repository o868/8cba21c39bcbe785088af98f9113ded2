import { List, ThemeIcon } from '@mantine/core';
import React from 'react';
import { ArrowRightCircle } from 'tabler-icons-react';

const ListItem = (props) => {
  const { data } = props;

  return (
    <List
      spacing="xs"
      size="sm"
      center
      icon={
        <ThemeIcon color="teal" size={24} radius="xl">
          <ArrowRightCircle size={16} />
        </ThemeIcon>
      }
    >
      {data.map((item, index) => {
        return <List.Item key={index}>{item.name}</List.Item>;
      })}
    </List>
  );
};

export default ListItem;
