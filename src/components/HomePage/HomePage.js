import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Grid, LoadingOverlay, Title } from '@mantine/core';
import { useTranslation } from 'react-i18next';
import CardItem from '../Parts/CardItem';
import useFetch from '../hooks/useFetch';

const HomePage = () => {
  const { t } = useTranslation();
  const loader = useRef(null);
  const [page, setPage] = useState(1);
  const { loading, error, products } = useFetch(page);

  const handleObserver = useCallback((entries) => {
    const target = entries[0];
    if (target.isIntersecting) {
      setPage((prev) => prev + 1);
    }
  }, []);

  useEffect(() => {
    const option = {
      root: null,
      rootMargin: '20px',
      threshold: 0,
    };
    const observer = new IntersectionObserver(handleObserver, option);
    if (loader.current) observer.observe(loader.current);
  }, [handleObserver]);

  return (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('products')}
      </Title>
      <Grid id="scrollableDiv" gutter="xl">
        {(products || []).map((item, index) => (
          <Grid.Col xs={12} md={6} lg={4} span={12} key={index}>
            <CardItem error={error} item={item} />
          </Grid.Col>
        ))}
        <LoadingOverlay visible={loading} />
      </Grid>
      <div ref={loader} />
    </>
  );
};

export default HomePage;
