import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Paper,
  Title,
  Grid,
  Image,
  Input,
  Button,
  ThemeIcon,
  Group,
  Badge,
  Divider,
  Tabs,
  Blockquote,
  Text,
} from '@mantine/core';
import { useTranslation } from 'react-i18next';
import {
  ArrowRightCircle,
  CircleCheck,
  CurrencyDollar,
  Search,
  Signature,
} from 'tabler-icons-react';
import { useSelector } from 'react-redux';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Pagination } from 'swiper';

const Detail = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const productsGlobal = useSelector((state) => state.products.products);
  const [detail, setDetail] = useState({
    isLoading: false,
    selected: history.location.state?.item ?? null,
  });
  const [searchText, setSearchText] = useState('');
  const [searchData, setSearchData] = useState([]);
  const itemId = history.location.pathname.split('/')[2];

  return itemId === undefined ? (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('search')}
      </Title>
      <Grid sx={{ marginBottom: '2rem' }}>
        <Grid.Col span={10}>
          <Input
            icon={<Search size={16} />}
            placeholder={t('search')}
            rightSectionWidth={70}
            styles={{ rightSection: { pointerEvents: 'none' } }}
            onChange={(event) => {
              setSearchText(event.target.value);
            }}
          />
        </Grid.Col>
        <Grid.Col span={2}>
          <Button
            variant="light"
            color="blue"
            fullWidth
            disabled={searchText === ''}
            onClick={() => {
              const findSearch = productsGlobal.filter((item) => {
                return item.title.includes(searchText);
              });
              setSearchData(findSearch);
            }}
          >
            {t('search')}
          </Button>
        </Grid.Col>
      </Grid>
      <Paper shadow="md" radius="md" p="md">
        {searchData.length > 0 ? (
          searchData.map((item, index) => {
            return (
              <Grid sx={{ alignItems: 'center' }} key={index}>
                <Grid.Col span={2}>
                  <ThemeIcon color="teal" size={24} radius="xl">
                    <CircleCheck size={16} />
                  </ThemeIcon>
                </Grid.Col>
                <Grid.Col span={8}>{item.title}</Grid.Col>
                <Grid.Col span={2}>
                  <Button
                    variant="light"
                    color="blue"
                    fullWidth
                    onClick={() => {
                      history.push(`/detail/${item.id}`, { item: item });
                      setDetail((prevState) => ({
                        ...prevState,
                        selected: item,
                      }));
                    }}
                  >
                    {t('detail')}
                  </Button>
                </Grid.Col>
              </Grid>
            );
          })
        ) : (
          <Text>{t('noData')}</Text>
        )}
      </Paper>
    </>
  ) : (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('detail')}
      </Title>
      <Paper shadow="md" radius="md" p="md">
        <Grid>
          <Grid.Col span={4}>
            <Swiper
              pagination={{
                dynamicBullets: true,
              }}
              modules={[Pagination]}
            >
              {detail?.selected?.images.map((item, index) => {
                return (
                  <SwiperSlide key={index}>
                    <Image
                      src={item.src}
                      height={500}
                      alt={item?.title}
                      fit="contain"
                    />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Grid.Col>
          <Grid.Col span={8}>
            <Group position="apart">
              <Title order={1}>{detail?.selected?.title ?? ''}</Title>
              <Badge
                variant="light"
                color="green"
                text={detail?.selected?.tags}
              >
                {detail?.selected?.tags}
              </Badge>
            </Group>
            <Divider my="sm" variant="dashed" />
            <Title sx={{ marginBottom: '1rem' }} order={3}>
              {t('variants')}
            </Title>
            <Tabs variant="pills" tabPadding="md">
              {detail?.selected?.variants.map((item, index) => {
                return (
                  <Tabs.Tab
                    key={index}
                    label={item.title}
                    icon={<Signature size={14} />}
                  >
                    <Blockquote
                      cite={t('discountPrice')}
                      icon={<CurrencyDollar size={24} />}
                    >
                      {item?.price}
                    </Blockquote>
                  </Tabs.Tab>
                );
              })}
            </Tabs>
            <Divider my="sm" variant="dashed" />
            <Title sx={{ marginBottom: '1rem' }} order={3}>
              {t('vendor')}
            </Title>
            <Group position="apart">
              <ThemeIcon color="teal" size={24} radius="xl">
                <ArrowRightCircle size={16} />
              </ThemeIcon>
              <Title order={5}>{detail?.selected?.vendor ?? ''}</Title>
            </Group>
          </Grid.Col>
        </Grid>
      </Paper>
    </>
  );
};

export default Detail;
