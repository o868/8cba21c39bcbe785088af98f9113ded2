import styled from 'styled-components';

export const SHeader = styled.div`
  width: 100%;
`;

export const TR = styled.div`
  content: 'TR';
`;

export const EN = styled.div`
  content: 'EN';
`;
