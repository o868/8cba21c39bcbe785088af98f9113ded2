import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { decipher } from '../../libraries/cipherDeChipher';
import { useDispatch } from 'react-redux';
import { productsActions } from '../../app/slices/ProductsSlice';

function useFetch() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();
  const [products, setProducts] = useState([]);
  const myDecipher = decipher('mySecretSalt');
  const hideText = myDecipher('7c6d6366697b716766');

  const sendQuery = useCallback(async () => {
    try {
      setLoading(true);
      setError(false);
      let data = {
        params: {
          // page: page,
          // limit: 10,
        },
        headers: {
          'X-Access-Token': process.env.REACT_APP_API_KEY,
          'content-type': 'application/json',
        },
      };
      const res = await axios.get(
        `https://${hideText}.netlify.app/.netlify/functions/products`,
        {
          ...data,
        }
      );
      setProducts((prev) => [
        ...new Set([...prev, ...(res?.data?.products ?? [])]),
      ]);
      dispatch(productsActions.addProducts(res?.data?.products));
      setLoading(false);
    } catch (err) {
      setError(err);
    }
  }, []);

  useEffect(() => {
    sendQuery();
  }, []);

  return { loading, error, products };
}

export default useFetch;
