import { configureStore } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import uiSlice from './slices/uiSlice';
import AuthSlice from './slices/AuthSlice';
import langSlice from './slices/langSlice';
import favoriteSlice from './slices/FavoriteSlices';
import ProductsSLice from './slices/ProductsSlice';

const reducers = combineReducers({
  ui: uiSlice.reducer,
  auth: AuthSlice.reducer,
  lang: langSlice.reducer,
  favorite: favoriteSlice.reducer,
  products: ProductsSLice.reducer,
});

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [thunk],
});

export default store;
