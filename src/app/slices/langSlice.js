import { createSlice } from '@reduxjs/toolkit';

const langSlice = createSlice({
  name: 'language',
  initialState: {
    currentLang: localStorage.getItem('language') ?? 'tr',
  },
  reducers: {
    toggleLanguage(state) {
      state.currentLang = state.currentLang === 'tr' ? 'en' : 'tr';
    },
  },
});

export const langActions = langSlice.actions;
export default langSlice;
