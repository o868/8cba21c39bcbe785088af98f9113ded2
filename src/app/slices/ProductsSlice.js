import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  products: [],
};

export const ProductsSLice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    addProducts: (state, action) => {
      state.products = [...action.payload];
    },
  },
});

export const productsActions = ProductsSLice.actions;

export default ProductsSLice;
