import React, { createContext } from 'react';
import { useSelector } from 'react-redux';
const ThemeProviderContext = createContext();

export const ThemeProviderContextTheme = ({ children }) => {
  const theme = useSelector((state) => state.ui.theme);
  const value = {
    theme,
  };

  return (
    <ThemeProviderContext.Provider value={value}>
      {children}
    </ThemeProviderContext.Provider>
  );
};

export default ThemeProviderContext;
