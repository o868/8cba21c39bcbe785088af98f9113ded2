import Box from './Box';

const Auth = {
  rehydratereToken: () => {
    Box.auth.isValid = true;
    Box.auth.isApiValid = true;
  },
  logout: () => {
    Box.auth.isValid = false;
    Box.auth.isApiValid = false;
  },
};

export default Auth;
