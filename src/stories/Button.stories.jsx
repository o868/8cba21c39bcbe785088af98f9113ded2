import React from 'react';
import { Database } from 'tabler-icons-react';

import Button from '../components/Parts/ButtonItem';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Button',
};

export const Outline = Template.bind({});
Outline.args = {
  variant: 'outline',
  label: 'Button',
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  size: 'large',
  leftIcon: <Database />,
  label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Button',
};
