import React from 'react';

import { Card } from '@mantine/core';

export default {
  title: 'Card',
  component: <Card></Card>,
};

const Template = (args) => <Card {...args} />;

export const WithShadow = Template.bind({});
WithShadow.args = {
  shadow: 'xl',
};

export const WithBorder = Template.bind({});
WithBorder.args = {
  withBorder: true,
};
